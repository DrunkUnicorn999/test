import time


element = 0
data = []

while True:
    try:
        element = input()
        if element.startswith('```') or element == '\n':
            continue
        data.append(element)
    except EOFError:
        break

while True:
    for i in data:
        try:
            if i.startswith("#"):
                i = ''
                print('\033c', end="")  # очищает терминал (end="" не позволяет напечатать новую строку)
                time.sleep(0.6)

            print(f"\033[35m {i}")
        except KeyboardInterrupt:
            print(f'\033[35mВы завершили показ gif-фнимации...')
            exit()
