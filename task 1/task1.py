element = count = 0
total = 42

while element is not False:

    try:
        element = int(input())
    except ValueError:
        print(f'Были обнаружены недопустимые символы на строке {count}')
        count += 1
        continue
    except EOFError:
        print('Конец')
        break
    try:
        total += 1/element
    except ZeroDivisionError:
        print(f'Были обнаружен 0 на строке {count}')
        count += 1
        continue

print(f'Сумма членов последовательности = {total}')
